#include "teapotscene.h"

namespace mrxben001
{
    // Store a limited version of PI for radian calculations
    const float PI = 3.14159265;

    // Singleton instance of teapotscene to support static GLUT window callbacks
    TeapotScene *TeapotScene::instance = NULL;

    // Constructor
    TeapotScene::TeapotScene()
    {
        // defaults
        title = "Teapot Scene";
        width = 800;
        height = 600;

        // buttons are up
        leftbutton = false;
        rightbutton = false;

        // by default lights should move
        movelights = true;
    }

    // Destructor - empty
    TeapotScene::~TeapotScene() { }

    // Set the singleton instance
    void TeapotScene::setInstance()
    {
        instance = this;
    }

    // set window resolution (before init()))
    void TeapotScene::setResolution(int w, int h)
    {
        width = w;
        height = h;
    }

    // set window title (before init())
    void TeapotScene::setTitle(std::string t)
    {
        title = t;
    }

    // initialise the window and scene
    void TeapotScene::init(int argc, char *argv[])
    {
        setInstance();

        // Create the window
        glutInit(&argc, argv);
        glutInitDisplayMode(GLUT_RGBA | GLUT_DOUBLE | GLUT_DEPTH);
        glutInitWindowPosition(100, 100);
        glutInitWindowSize(width, height);
        glutCreateWindow(title.c_str());

        // set cursor
        glutSetCursor(GLUT_CURSOR_INFO);

        // Bind static window events
        glutReshapeFunc(reshapeWrapper);
        glutMouseFunc(mouseButtonPressWrapper);
        glutMotionFunc(mouseMoveWrapper);
        glutDisplayFunc(displayWrapper);
        glutKeyboardFunc(keyboardDownWrapper);
        glutKeyboardUpFunc(keyboardUpWrapper);

        // call update every 16ms
        glutTimerFunc(16, updateWrapper, 0);

        // Set teapot texture
        teapot.setTexture("mosaic.raw", 512, 512);
        teapot.setBumpMap("perlin_noise.raw", 256, 256);

        // create the camera
        camera = Camera(6.0f,PI/4, PI/4);

        // create lights
        setupLights();

        // Setup Depth
        glEnable(GL_DEPTH_TEST);
        glDepthFunc(GL_LEQUAL);

        // Setup Texturing
        glEnable( GL_COLOR_MATERIAL );
        glMaterialf(GL_FRONT, GL_SHININESS, 100.0);
        GLfloat specReflection[] = { 0.9, 0.9, 0.9, 1.0 };
        glMaterialfv(GL_FRONT, GL_SPECULAR, specReflection);

        // Setup Projection Mode
        glMatrixMode(GL_PROJECTION);
        glLoadIdentity();
        gluPerspective(45, ((GLfloat) width / height), 1.0f, 500.0f);
        glHint( GL_PERSPECTIVE_CORRECTION_HINT, GL_NICEST );

        // Setup Matrix Mode
        glMatrixMode(GL_MODELVIEW);
        glLoadIdentity();
        glViewport(0, 0, width, height);

        // Create and link the shaders
        initShaders();

        glUniform1i(glGetUniformLocation(shaderControl.program, "baseTexture"), 0);
        glUniform1i(glGetUniformLocation(shaderControl.program, "bumpMapTexture"), 1);

        MY_GL_TEXTURE = glGetUniformLocation(shaderControl.program, "enableTexture");
        MY_GL_BUMPMAP = glGetUniformLocation(shaderControl.program, "enableBumpMap");
        MY_GL_TEXTUREANDLIGHT = glGetUniformLocation(shaderControl.program, "enableTextureAndLight");

        glUniform1i(MY_GL_TEXTURE, GL_TRUE);
        glUniform1i(MY_GL_BUMPMAP, GL_TRUE);
        glUniform1i(MY_GL_TEXTUREANDLIGHT, GL_TRUE);
    }

    // Load, Compile, Link and Activate the vertex and fragment shader
    void TeapotScene::initShaders()
    {
        shaderControl.init();
        shaderControl.loadShader(GL_VERTEX_SHADER, "vPhong.glsl");
        shaderControl.loadShader(GL_FRAGMENT_SHADER, "fPhong.glsl");
        shaderControl.link();
    }

    // Start the window loop and start firing Func callbacks
    void TeapotScene::start()
    {
        glutMainLoop();
    }

    // Create the list of light objects
    void TeapotScene::setupLights()
    {
        // Create the 3 lights
        Light light0(GL_LIGHT0);
        light0.setSphericalPosition(6.0f, 0, 0);
        light0.setDiffuse(0.0f, 0.0f, 0.5f);            // Blue
        light0.setSpecular(0.1f, 0.1f, 1.0f);           // Blue
        lights.push_back( light0 );

        Light light1(GL_LIGHT1);
        light1.setSphericalPosition(6.0f, 0, -PI*2/3);
        light1.setDiffuse(0.5f, 0.0f, 0.0f);            // Red
        light1.setSpecular(1.0f, 0.1f, 0.1f);           // Red
        lights.push_back( light1);

        Light light2(GL_LIGHT2);
        light2.setSphericalPosition(6.0f, 0, PI*2/3);
        light2.setDiffuse(0.5f, 0.5f, 0.0f);            // Yellow
        light2.setSpecular(1.0f, 1.0f, 0.1f);           // Yellow
        lights.push_back( light2 );

        // Enable lights
        glEnable(GL_LIGHTING);

        // Globale light model
        GLfloat amb_light[] = { 0.0f, 0.0f, 0.0f, 1.0f };
        glLightModelfv( GL_LIGHT_MODEL_AMBIENT, amb_light );
        glLightModeli( GL_LIGHT_MODEL_TWO_SIDE, GL_FALSE );

        // Enable each light and apply position/diffuse/specular
        for(uint i=0;i<lights.size();++i)
        {
            lights[i].enable();
            lights[i].apply();
        }
    }

    void TeapotScene::toggleTexture()
    {
        GLint isEnabled;
        glGetUniformiv(shaderControl.program, MY_GL_TEXTURE, &isEnabled);
        if(isEnabled)
        {
            glUniform1i(MY_GL_TEXTURE, GL_FALSE);
        }
        else
        {
            glUniform1i(MY_GL_TEXTURE, GL_TRUE);
        }
    }

    void TeapotScene::toggleBumpMap()
    {
        GLint isEnabled;
        glGetUniformiv(shaderControl.program, MY_GL_BUMPMAP, &isEnabled);
        if(isEnabled)
        {
            glUniform1i(MY_GL_BUMPMAP, GL_FALSE);
        }
        else
        {
            glUniform1i(MY_GL_BUMPMAP, GL_TRUE);
        }
    }

    void drawText(const char *text, int x, int y)
    {
        glRasterPos2f(x,y);
        const char *c;
        for (c=text; *c != '\0'; c++)
        {
            glutBitmapCharacter(GLUT_BITMAP_HELVETICA_12, *c);
        }
    }

    void TeapotScene::drawHud()
    {
        glMatrixMode(GL_PROJECTION);
            glPushMatrix();
                glLoadIdentity();
                glOrtho(0.0f, width, height, 0.0f, -1.0f, 10.0f);

                glMatrixMode(GL_MODELVIEW);
                glPushMatrix();
                    glLoadIdentity();


                    glUniform1i(MY_GL_TEXTUREANDLIGHT, GL_FALSE);
                    glDisable(GL_DEPTH_TEST);

                    // Helps when spacing the lines
                    int line = 1;
                    int lineheight = 14;

                    glColor4f(1.0f, 1.0f, 1.0f,1.0f);
                    drawText("CONTROLS:", 10, lineheight*line); line++;
                    drawText("============", 10, lineheight*line); line++;

                    line++;
                    drawText("Rotate Camera: Left mouse button", 10, lineheight*line++);
                    drawText("Zoom Camera: Right mouse button", 10, lineheight*line++);
                    line++;
                    drawText("Toggle Texture: [t]", 10, lineheight*line++);
                    drawText("Toggle BumpMap: [b]", 10, lineheight*line++);
                    drawText("Toggle Moving Lights: [m]", 10, lineheight*line++);

                    glEnable(GL_DEPTH_TEST);

                    glUniform1i(MY_GL_TEXTUREANDLIGHT, GL_TRUE);

                glPopMatrix();
                glMatrixMode(GL_PROJECTION);

            glPopMatrix();
            glMatrixMode(GL_MODELVIEW);

    }

    void TeapotScene::drawAxes()
    {
        glUniform1i(MY_GL_TEXTUREANDLIGHT, GL_FALSE);
        glLineWidth(1.0f);
        glColor4f(0.2f,0.2f,0.2f, 1.0f);
        glBegin(GL_LINES);
        glVertex3f(-10, 0, 0);
        glVertex3f(10,0,0);
        glVertex3f(0,-10, 0);
        glVertex3f(0,10,0);
        glVertex3f(0,0,-10);
        glVertex3f(0,0,10);
        glEnd();
        glUniform1i(MY_GL_TEXTUREANDLIGHT, GL_TRUE);
    }

    // The main DRAW method
    void TeapotScene::display()
    {
         //clears colour and depth buffers
        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

        glLoadIdentity();

        // Add the camera matrix transform to the matrix stack.
        camera.apply();

        // Draw the 3 axes
        drawAxes();

        // apply position/diffus/specular for each light
        for(uint i=0;i<lights.size();++i)
        {
            lights[i].apply();
        }

        // draw the teapot
        teapot.draw();

        drawHud();

        // Double buffered!
        glutSwapBuffers();
    }

    // Window resize call
    void TeapotScene::reshape(int width, int height)
    {
        // Change the viewport dimensions to match the window
        glViewport(0, 0, width, height);
        glMatrixMode(GL_PROJECTION);
        glLoadIdentity();
        // new dimensions means new aspect ratio
        gluPerspective(45, ((GLfloat) width / height), 1.0f, 500.0f);   // new perspective
        glMatrixMode(GL_MODELVIEW);

        // repaint the screen
        glutPostRedisplay();
    }

    // Mouse button press event
    void TeapotScene::mouseButtonPress(int button, int state, int x, int y)
    {
        // store mouse variables for use in the mouseMove() event
        mouse_click_pos[0] = x;
        mouse_click_pos[1] = y;
        leftbutton = (button == GLUT_LEFT_BUTTON && state == GLUT_DOWN);
        rightbutton = (button == GLUT_RIGHT_BUTTON && state == GLUT_DOWN);
    }

    // Mouse move when buttons are down
    void TeapotScene::mouseMove(int x, int y)
    {
        // Left button controls spherical movement
        if( leftbutton )
        {
            camera.yaw =   ( camera.yaw + ( GLfloat ) ( x - mouse_click_pos[0] ) / 300.0 ) ;
            camera.pitch = ( camera.pitch + ( GLfloat ) ( mouse_click_pos[1] - y ) / 300.0 );

            // clamp
            if (camera.pitch < 0.001) camera.pitch = 0.001;
            if (camera.pitch > (PI-0.001)) camera.pitch = PI-0.001;
        }

        // Right button controls zoom
        if( rightbutton )
        {
            camera.distance = (camera.distance + (GLfloat) (mouse_click_pos[1] - y) / 100);

            // clamp
            if (camera.distance < 3) camera.distance = 3;
            if (camera.distance > 30) camera.distance = 30;
        }

        // Store current position
        mouse_click_pos[0] = x;
        mouse_click_pos[1] = y;

        // convert camera spherical coords to world coords
        camera.rebuildPosition();

        // repaint the screen
        glutPostRedisplay();
    }

    void TeapotScene::keyboardDown( unsigned char key, int x, int y )
    {

    }

    void TeapotScene::keyboardUp( unsigned char key, int x, int y )
    {
        if (key == 't')
        {
            toggleTexture();
        }

        if (key == 'b')
        {
            toggleBumpMap();
        }

        if (key == 'm')
        {
            movelights = (movelights == false);
        }




    }

    // Update loop
    void TeapotScene::update()
    {
        if(movelights)
        {

            // rotate all of the lights around the Y axis
            for(uint i=0;i<lights.size();++i)
            {
                lights[i].yaw += 0.1f;
                lights[i].rebuildPosition();
            }

            // fix light 0 and just change its pitch
            lights[0].yaw = 0.0f;
            lights[0].pitch += 0.1f;

        }

        //repaint
        glutPostRedisplay();
    }


    // Static wrapper methods used by the GLUT window callbacks

    void TeapotScene::displayWrapper()
    {
        instance->display();
    }

    void TeapotScene::reshapeWrapper(int width, int height)
    {
        instance->reshape(width, height);
    }

    void TeapotScene::mouseButtonPressWrapper(int button, int state, int x, int y)
    {
        instance->mouseButtonPress(button, state, x, y);
    }

    void TeapotScene::mouseMoveWrapper(int x, int y)
    {
        instance->mouseMove(x, y);
    }

    void TeapotScene::keyboardDownWrapper(unsigned char key, int x, int y)
    {
        instance->keyboardDown(key,x,y);
    }

    void TeapotScene::keyboardUpWrapper(unsigned char key, int x, int y)
    {
        instance->keyboardUp(key,x,y);
    }

    void TeapotScene::updateWrapper(int v)
    {
        instance->update();
        glutTimerFunc(16, updateWrapper, 0); // call it again later
    }

}

