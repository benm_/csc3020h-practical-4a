#ifndef CAMERA_H
#define CAMERA_H

#include <GL/glut.h>
#include <math.h>

namespace mrxben001
{
    class Camera
    {
        public:
            Camera();
            Camera(float distance, float pitch, float yaw);
            virtual ~Camera();

            void apply();
            void rebuildPosition();

            float position[3];
            float distance;
            float yaw;
            float pitch;
    };
}

#endif // CAMERA_H
