OBJS = main.o camera.o light.o shadercontrol.o teapotobj.o teapotscene.o
CC = g++
DEBUG = -g
CFLAGS = -DGL_GLEXT_PROTOTYPES $(DEBUG)
LFLAGS = $(DEBUG) -lX11 -lglut -lGL -lGLU
TARGET = shaderprac
TARBALL = shaderprac.tar.gz


all: $(OBJS)
	$(CC) $(LFLAGS) $(OBJS) -o $(TARGET)

main.o: main.cpp main.h M4x4.h
	$(CC) $(CFLAGS) -c main.cpp

camera.o: camera.cpp camera.h
	$(CC) $(CFLAGS) -c camera.cpp

light.o: light.cpp light.h
	$(CC) $(CFLAGS) -c light.cpp

shadercontrol.o: shadercontrol.cpp shadercontrol.h
	$(CC) $(CFLAGS) -c shadercontrol.cpp

teapotobj.o: teapotobj.cpp teapotobj.h
	$(CC) $(CFLAGS) -c teapotobj.cpp

teapotscene.o: teapotscene.cpp teapotscene.h
	$(CC) $(CFLAGS) -c teapotscene.cpp

clean:
	rm -f *.o
	rm -f $(TARGET)
	rm -f $(TARBALL)

tar:
	rm -f $(TARBALL)
	tar zcvf $(TARBALL) *.cpp *.h *.raw *.glsl makefile README




