#ifndef SHADERCONTROL_H
#define SHADERCONTROL_H


#include <stdio.h>
#include <stdlib.h>
#include <GL/gl.h>
#include <GL/glut.h>
#include <GL/glext.h>
#include <sys/stat.h>
#include <cstring>
#include <iostream>

namespace mrxben001
{
    class ShaderControl
    {
        public:
            ShaderControl();
            virtual ~ShaderControl();

            void init();
            void loadShader(GLint type, const char * file);
            void link();

            static GLint buildShaderFromFile(GLuint type, const char* filename);

            GLuint program;
    };
}

#endif // SHADERCONTROL_H
