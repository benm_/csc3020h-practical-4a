// These values must be provided to the fragment shader
// All of these will be interpolated (varied) across the primitive
varying vec3 normal, vpos;

void main()
{
    // First set position of the vertex, this is easy
    gl_Position = gl_ProjectionMatrix * gl_ModelViewMatrix * gl_Vertex ;

    // calculate the normal at the vertex
    normal = normalize(gl_NormalMatrix * gl_Normal);

    // position of the vertex
    vpos = vec3(gl_ModelViewMatrix * gl_Vertex);

    // set texture coordinate
    gl_TexCoord[0] = gl_MultiTexCoord0;

    // Get value of glColor
    gl_FrontColor = gl_Color;

}
