#ifndef TEAPOTOBJ_H
#define TEAPOTOBJ_H

#include <stdio.h>
#include <stdlib.h>
#include <GL/gl.h>
#include <GL/glut.h>
#include <GL/glext.h>
#include <sys/stat.h>
#include <cstring>
#include <iostream>
#include "M4x4.h"

namespace mrxben001
{
    class TeapotObj
    {
        public:
            TeapotObj();
            virtual ~TeapotObj();

            void setTexture(const char * file, int width, int height);
            void setBumpMap(const char * file, int width, int height);
            void resetTransform();

            void draw();
        protected:
        private:
            M4x4 local;
            M4x4 global;
            GLuint texture;
            GLuint bumpmap;
    };
}

#endif // TEAPOTOBJ_H
