#include "light.h"

namespace mrxben001
{
    Light::Light() { }

    // constructor with GL_LIGHT id
    Light::Light(int id)
    {
        glID = id;
    }

    Light::~Light() { }

    // Enable the GL light
    void Light::enable()
    {
        glEnable(glID);
    }

    // Apply the position, and colour information to the GL light
    void Light::apply()
    {
        glLightfv(glID, GL_POSITION, position);
        glLightfv(glID, GL_DIFFUSE, diffusecolour);
        glLightfv(glID, GL_SPECULAR, specularcolour);
    }

    // Set spherical coordinates
    void Light::setSphericalPosition(float d, float p, float y)
    {
        distance = d;
        pitch = p;
        yaw = y;
        rebuildPosition();
    }

    // Convert spherical to world coords
    void Light::rebuildPosition()
    {
        position[0] = distance * cosf(pitch) * cosf(yaw);
        position[1] = distance * sinf(pitch);
        position[2] = distance * sinf(yaw) * cosf(pitch);
    }

    // Set diffuse colour components
    void Light::setDiffuse(float r, float g, float b)
    {
        diffusecolour[0] = r;
        diffusecolour[1] = g;
        diffusecolour[2] = b;
    }

    // Set specular colour components
    void Light::setSpecular(float r, float g, float b)
    {
        specularcolour[0] = r;
        specularcolour[1] = g;
        specularcolour[2] = b;
    }
}
