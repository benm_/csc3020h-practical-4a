#include "main.h"



using namespace std;

int main(int argc, char* argv[])
{
    // Create a new teapot scene
    mrxben001::TeapotScene* ts = new mrxben001::TeapotScene();
    // window
    ts->setResolution(800,600);
    // title
    ts->setTitle("MRXBEN001 : Phong Teapot");
    // initialise
    ts->init(argc, argv);
    // start mainloop
    ts->start();

    return 0;
}
