#include "camera.h"


namespace mrxben001
{
    // blank constuct
    Camera::Camera() { }

    // initialise spherical coords
    Camera::Camera(float d, float p, float y)
    {
        distance = d;
        pitch = p;
        yaw = y;

        rebuildPosition();
    }

    // blank destruct
    Camera::~Camera() { }

    // Apply the camera to the current worldview
    void Camera::apply()
    {
        gluLookAt(position[0],position[1],position[2], 0, 0, 0, 0, 1, 0);
    }

    // convert spherical to world coords
    void Camera::rebuildPosition()
    {
        position[0] = distance*sin(pitch)*cos(yaw);
        position[1] = distance*cos(pitch);
        position[2] = distance*sin(pitch)*sin(yaw);
    }
}
