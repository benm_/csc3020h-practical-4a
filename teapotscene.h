#ifndef TEAPOTSCENE_H
#define TEAPOTSCENE_H

#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <GL/glut.h>

#include <iostream>
#include <math.h>
#include <vector>

#include "teapotobj.h"
#include "camera.h"
#include "light.h"
#include "shadercontrol.h"

namespace mrxben001
{
   class TeapotScene
    {

        protected:
            static TeapotScene * instance;

        public:
            TeapotScene();
            virtual ~TeapotScene();

            void setInstance();
            void setResolution(int w, int h);
            void setTitle(std::string t);
            void init(int argc, char *argv[]);

            void start();

        protected:
            void display();
            void reshape(int width, int height);
            void mouseButtonPress(int button, int state, int x, int y);
            void mouseMove(int x, int y);
            void keyboardDown( unsigned char key, int x, int y );
            void keyboardUp( unsigned char key, int x, int y );
            void update();

            // static wrapper methods for glut callbacks
            static void displayWrapper();
            static void reshapeWrapper(int width, int height);
            static void mouseButtonPressWrapper(int button, int state, int x, int y);
            static void mouseMoveWrapper(int x, int y);
            static void keyboardDownWrapper(unsigned char key, int x, int y);
            static void keyboardUpWrapper(unsigned char key, int x, int y);
            static void updateWrapper(int v);

        private:

            void initShaders();
            void setupLights();

            void toggleTexture();
            void toggleBumpMap();

            void drawHud();
            void drawAxes();

            // window variables
            int width;
            int height;
            std::string title;

            // scene objects
            TeapotObj teapot;
            Camera camera;

            // list of lights
            std::vector<Light> lights;
            bool movelights;

            // mouse event holders
            bool leftbutton;
            bool rightbutton;
            float mouse_click_pos[2];

            // shaders
            ShaderControl shaderControl;

            GLint MY_GL_TEXTURE;
            GLint MY_GL_BUMPMAP;
            GLint MY_GL_TEXTUREANDLIGHT;
    };
}


#endif // TEAPOTSCENE_H
