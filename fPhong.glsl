// ALWAYS HARDCODE NUMBER OF LIGHTS!
#define NUM_LIGHTS 3

// These values have been provided by the vertex shader
varying vec3 normal, vpos;

uniform sampler2D baseTexture;
uniform bool enableTexture;

uniform sampler2D bumpMapTexture;
uniform bool enableBumpMap;

uniform bool enableTextureAndLight;

void main()
{

    // This variable allows lighting and texturing to be switched off. Useful for drawing the HUD text
    if (enableTextureAndLight)
    {
        // base colour is black
        vec4 colour = vec4(0.0,0.0,0.0,0.0);

        // bump deflection is by default 0
        vec3 bump = vec3(0.0,0.0,0.0);

        // if bump mapping is enabled, get the value from the texture
        if(enableBumpMap) bump = normalize( texture2D(bumpMapTexture, gl_TexCoord[0].st).xyz * 2.0 - 1.0);

        // combine
        vec3 N = normalize(normal + bump);

        // For each light, compute colour components
        for(int i=0;i<NUM_LIGHTS;++i)
        {
            // compute vector from vertex to light
            vec3 L = normalize(gl_LightSource[i].position.xyz - vpos);
            vec3 E  = normalize(-vpos);
            vec3 R = normalize(-reflect(L, N));

            // ambient component
            vec4 ambient = gl_FrontLightProduct[i].ambient;

            // diffuse component
            vec4 diffuse = gl_FrontLightProduct[i].diffuse * max(dot(N, L), 0.0);
            diffuse = clamp(diffuse, 0.0, 1.0);

            // specular component
            vec4 specular = gl_FrontLightProduct[0].specular * pow(max(dot(R, E), 0.0), 0.3 * gl_FrontMaterial.shininess);
            specular = clamp(specular, 0.0, 1.0);

            // add to current light colour
            colour += ambient + diffuse + specular;
        }

        // if texturing is enabled, get the texture colour
        vec4 texColor = vec4(0.0,0.0,0.0,1.0);
        if(enableTexture) texColor = texture2D(baseTexture, gl_TexCoord[0].st) * 0.5;

        // combine into final colour
        gl_FragColor = texColor + gl_FrontLightModelProduct.sceneColor + colour;

    }
    else
    {
        // if light and texture is not enabled, set the final colour to the value of glColor
        gl_FragColor = gl_Color;

    }
}
