#include "shadercontrol.h"

/*
 A useful class for reading, compiling and linking shaders
*/
namespace mrxben001
{
    ShaderControl::ShaderControl() { }

    ShaderControl::~ShaderControl() {}

    // create program id
    void ShaderControl::init()
    {
        program = glCreateProgram();
    }

    // load a shader, this just attaches the shader and does not link it in the end
    void ShaderControl::loadShader(GLint type, const char * file)
    {
        GLuint shader = buildShaderFromFile(type, file);
        // If it read in successfully
        if(shader !=0)
        {
            glAttachShader(program, shader);
            glDeleteShader(shader); // just free it
        }

    }

    // link the shaders in
    // checks to see if the linking was successful (valid shaders)
    void ShaderControl::link()
    {
        GLint result;
        // link program
        glLinkProgram(program);
        // check the results
        glGetProgramiv(program, GL_LINK_STATUS, &result);

        if(result == GL_FALSE)
        {
            // if it failed to link, read the error log and exit
            GLint length;
            glGetProgramiv(program, GL_INFO_LOG_LENGTH, &length);
            // reserve buffer
            char* log = (char*) malloc(length);
            glGetProgramInfoLog(program, length, &result, log);
            printf("link(): Program/Shader linking failed: %s\n", log);
            free(log);
            glDeleteProgram(program);
            program = 0;
        }

        // start using the new shader
        glUseProgram(program);
    }

    // load file into big char array
    static char* loadFile(const char* filename)
    {
        FILE* fp = fopen(filename, "r");

        // read file length
        struct stat statBuf;
        stat(filename, &statBuf);

        //reserve buffer
        char* buffer = (char*) malloc(statBuf.st_size + sizeof(char));
        size_t len = fread(buffer, 1, statBuf.st_size, fp);

        // set the null terminator
        buffer[len] = '\0';
        fclose(fp);

        return buffer;
    }

    // build / compile shader
    GLint ShaderControl::buildShaderFromFile(GLuint type, const char* filename)
    {

        // read file
        char* source = loadFile(filename);
        if (!source) return 0;

        // initialise shader
        GLuint shader = glCreateShader(type);
        GLint length = strlen(source);
        glShaderSource(shader, 1, (const  char**)&source, &length);

        // compile
        glCompileShader(shader);
        free(source);

        // check compilation result
        GLint result;
        glGetShaderiv(shader, GL_COMPILE_STATUS, &result);
        if(result == GL_FALSE)
        {
            glGetShaderiv(shader, GL_INFO_LOG_LENGTH, &length);
            char * log = (char*) malloc(length);
            glGetShaderInfoLog(shader, length, &result, log);

            printf("buildShaderFromFile(): unable to compile %s: %s\n", filename, log);
            free(log);

            glDeleteShader(shader);
            return 0;
        }
        return shader;
    }
}

