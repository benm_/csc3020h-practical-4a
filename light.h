#ifndef LIGHT_H
#define LIGHT_H

#include <GL/glut.h>
#include <math.h>

namespace mrxben001
{
    class Light
    {
        public:
            Light();
            Light(int id);
            virtual ~Light();

            void enable();
            void apply();
            void setSphericalPosition(float distance, float pitch, float yaw);
            void rebuildPosition();
            void setDiffuse(float r, float g, float b);
            void setSpecular(float r, float g, float b);


            int glID;
            float distance;
            float pitch;
            float yaw;
            float position[3];
            float diffusecolour[4];
            float specularcolour[4];
    };
}


#endif // LIGHT_H
