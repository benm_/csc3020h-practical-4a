#include "teapotobj.h"

namespace mrxben001
{
    TeapotObj::TeapotObj() { }

    TeapotObj::~TeapotObj() { }

    // Method to load and bind a texture
    GLuint loadTexture(const char * filename, int width, int height)
    {
        FILE * file = fopen(filename, "rb");
        if (file == NULL) return 0;

        // create buffer and read into it
        unsigned char* data = (unsigned char*) malloc(width * height * 3);
        size_t len = fread(data, 1, width * height *3, file);
        fclose(file);

        //check file resolution and buffer size
        if (len != (width * height * 3))
        {
            printf("Texture %s does not match the given resolution (%dx%d)!\n", filename, width, height);
            exit(EXIT_FAILURE);
        }

        // generate new open gl texture id
        GLuint tex;
        glGenTextures(1, &tex);

        glBindTexture(GL_TEXTURE_2D, tex);
        glTexEnvf(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_MODULATE);

        // Texture filter
        glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
        glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);

        // Texture wrapping
        glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
        glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);

        // set texture data
        glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, width, height, 0, GL_RGB, GL_UNSIGNED_BYTE, data);
        free(data);

        return tex;
    }

    void TeapotObj::setTexture(const char * file, int width, int height)
    {
        glActiveTexture(GL_TEXTURE0);
        texture = loadTexture(file, width, height);
    }

    void TeapotObj::setBumpMap(const char  * file, int width, int height)
    {
        glActiveTexture(GL_TEXTURE1);
        bumpmap = loadTexture(file, width, height);
    }

    // reset transform
    void TeapotObj::resetTransform()
    {
        local.load_identity();
        global.load_identity();
    }

    // Draw the teapot
    void TeapotObj::draw()
    {
        // store current world transform
        glPushMatrix();

            // White colour
            glColor3f(1.0, 1.0, 1.0);

            glActiveTexture(GL_TEXTURE1);
            glBindTexture(GL_TEXTURE_2D, bumpmap);

            glActiveTexture(GL_TEXTURE0);
            glBindTexture(GL_TEXTURE_2D, texture);

            // draw glut teapot
            glutSolidTeapot(1);

        glPopMatrix();
    }
}

